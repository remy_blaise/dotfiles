# dotfiles

## About

This repostiory contains default files that start with a `.`

## Gitlab Setup

There is a gitlab folder that can be used to create new gitlab repositories with the default files that it needs based on the project type. 

Make sure the following environment variables are setup in this repository or the group in which this repository resides for the build to run.  The current `.gitlab-ci.yml` deploys to the Docker Hub.

| Variable Name         | Variable Value                                            |
| --------------------  | --------------------------------------------------------- |
| DOCKER_REGISTRY_URL   | {docker.io}                                     			|
| CI_REGISTRY_USER   	| {dockerhub_username}        								|
| CI_REGISTRY_PASSWORD  | {dockerhub_password}                                      |
| GITLAB_ACCESS_TOKEN  	| {gitlab_token}                                      		|


* Create a new empty repository in gitlab through the web gui
* In the web gui click the create new file button
* Select a file type of .gitlab-ci.yml in the dropdown
* Select any .gitlab-ci.yml template so the file is named and pre-populated
* Override the pre-populated data with the information from below or from [here](https://gitlab.com/remy-graph/templates/dotfiles/raw/master/gitlab-ci/.default-ci.yml) replacing `{current-group}` with the group in which this repo resides

```
include:
  - project: 'remyhealth/template/dotfiles'
    file: '/gitlab-ci/.setup-ci.yml'
```
* In your commit message type `setup-` followed by one of the available setup examples:
    - setup-blank
    - setup-dotnet
    - setup-js
    - etc

* Once the build process completes a new `.gitlab-ci.yml` will be in the current repository
* Edit the `.gitlab-ci.yml` through the web gui replacing the required variables with your intended ones
* In the commit message start the message with the second half of your initial commit message
    - if the initial commit message was `setup-dotnet` then this new commit message will start with `dotnet-`
* The second half of this new commit message will depend on the type of project you created.
    - if the project only has one type then it doesn't matter what you put after it `dotnet-whatever`
    - if the project has subtypes you can fill those in. For example `dotnet-web` or `dotnet-nuget`